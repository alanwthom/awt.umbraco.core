﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Services;
using Umbraco.Web.HealthCheck;
using Umbraco.Web.HealthCheck.Checks.Config;

namespace AWT.Umbraco.Core.HealthChecks.Configuration
{
    [HealthCheck("06dbc34e-19e1-4f25-a47b-26f5ac402a08",
        "Image Processor Optimisation",
        Description = "Checks the preserveExifMetaData property is set to false so that ImageProcessor removes all metadata when optimising images.",
        Group = "Configuration")]
    public class PreserveExifMetaDataCheck : AbstractConfigCheck
    {
        private readonly ILocalizedTextService _textService;

        public PreserveExifMetaDataCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext)
        {
            _textService = healthCheckContext.ApplicationContext.Services.TextService;
        }

        public override string FilePath
        {
            get { return "~/Config/imageprocessor/processing.config"; }
        }

        public override string XPath
        {
            get { return "/processing/@preserveExifMetaData"; }
        }

        public override ValueComparisonType ValueComparisonType
        {
            get { return ValueComparisonType.ShouldEqual; }
        }

        public override IEnumerable<AcceptableConfiguration> Values
        {
            get
            {
                var values = new List<AcceptableConfiguration>
                {
                    new AcceptableConfiguration
                    {
                        IsRecommended = true,
                        Value = "false"
                    }
                };

                return values;
            }
        }

        public override string CheckSuccessMessage
        {
            get
            {
                return _textService.Localize("healthcheck/preserveExifMetaDataCheckSuccessMessage",
                    new[] { CurrentValue, Values.First(v => v.IsRecommended).Value });
            }
        }

        public override string CheckErrorMessage
        {
            get
            {
                return _textService.Localize("healthcheck/preserveExifMetaDataCheckErrorMessage",
                    new[] { CurrentValue, Values.First(v => v.IsRecommended).Value });
            }
        }

        public override string RectifySuccessMessage
        {
            get
            {
                return _textService.Localize("healthcheck/preserveExifMetaDataCheckRectifySuccessMessage",
                    new[] { Values.First(v => v.IsRecommended).Value });
            }
        }
    }
}

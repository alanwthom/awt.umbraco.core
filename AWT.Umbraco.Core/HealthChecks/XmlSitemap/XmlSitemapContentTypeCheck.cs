﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Umbraco.Core.Logging;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.XmlSitemap
{
    public class XmlSitemapContentTypeCheck
    {
        [HealthCheck(
            "4f9c3ad0-4d37-4de5-acc0-f626aa68c50c",
            "Sitemap.xml content type",
            Description = "Checks if the sitemap.xml file returns the correct content type",
            Group = "Sitemap.xml")]
        public class XmlSitemapExistsCheck : HealthCheck
        {
            public XmlSitemapExistsCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

            public override IEnumerable<HealthCheckStatus> GetStatus()
            {
                return new[] { CheckXmlSitemapContentType() };
            }

            public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
            {
                throw new NotImplementedException();
            }



            private HealthCheckStatus CheckXmlSitemapContentType()
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        var request = new HttpRequestMessage(HttpMethod.Head, $"{HealthCheckContext.SiteUrl}/sitemap.xml");
                        var response = client.SendAsync(request);

                        var contentTypeHeader = Enumerable.Empty<string>();
                        response.Result.Content.Headers.TryGetValues("Content-Type", out contentTypeHeader);
                        var contentType = string.Join(" ", contentTypeHeader);

                        return new HealthCheckStatus($"{HealthCheckContext.SiteUrl}/sitemap.xml returned a content type of <strong>{contentType}</strong>")
                        {
                            ResultType = response.Result.IsSuccessStatusCode && contentType.Contains("text/xml") ? StatusResultType.Success : StatusResultType.Error
                        };
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error<XmlSitemapExistsCheck>("An error occurred trying to retrieve the sitemap.xml file", ex);

                        return new HealthCheckStatus(
                            $"{HealthCheckContext.SiteUrl}/sitemap.xml returned an error. Please check your sitemap implementation.")
                        {
                            ResultType = StatusResultType.Error
                        };
                    }
                }
            }
        }
    }
}
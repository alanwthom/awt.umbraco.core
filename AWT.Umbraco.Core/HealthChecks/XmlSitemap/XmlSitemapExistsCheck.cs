﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Umbraco.Core.Logging;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.XmlSitemap
{
    [HealthCheck(
                "19020bf5-1977-47ef-b075-1e940f326094",
                "Sitemap.xml file exists",
                Description = "Checks to see if a sitemap.xml file exists",
                Group = "Sitemap.xml")]
    public class XmlSitemapExistsCheck : HealthCheck
    {
        public XmlSitemapExistsCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckXmlSitemapExists() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }



        private HealthCheckStatus CheckXmlSitemapExists()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Head, $"{HealthCheckContext.SiteUrl}/sitemap.xml");
                    var response = client.SendAsync(request);
                    var message = $"{HealthCheckContext.SiteUrl}/sitemap.xml returned a <strong>{(int) response.Result.StatusCode} {response.Result.StatusCode}</strong> status code.";

                    return new HealthCheckStatus(message)
                        {
                            ResultType = response.Result.IsSuccessStatusCode ? StatusResultType.Success : StatusResultType.Error
                        };
                }
                catch (Exception ex)
                {
                    LogHelper.Error<XmlSitemapExistsCheck>("An error occurred trying to retrieve the sitemap.xml file", ex);

                    return new HealthCheckStatus(
                        $"{HealthCheckContext.SiteUrl}/sitemap.xml returned an error. Please check your sitemap implementation.")
                    {
                        ResultType = StatusResultType.Error
                    };
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using AWT.Umbraco.Core.Models;
using Umbraco.Core.Logging;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.XmlSitemap
{
    [HealthCheck(
                "8746e48d-d50b-4d79-a979-e9feff8aa79b",
                "Sitemap.xml size",
                Description = "Checks the size of the sitemap.xml file",
                Group = "Sitemap.xml")]
    public class XmlSitemapSizeCheck : HealthCheck
    {
        public XmlSitemapSizeCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckXmlSitemapSize() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }



        private HealthCheckStatus CheckXmlSitemapSize()
        {
            using (var client = new WebClient())
            {
                try
                {
                    var xml = client.DownloadString($"{HealthCheckContext.SiteUrl}/sitemap.xml");
                    var reader = new StringReader(xml);
                    var serializer = new XmlSerializer(typeof(urlset));
                    var result = (urlset)serializer.Deserialize(reader);

                    return new HealthCheckStatus($"Xml sitemap contains {result.url.Length} pages.")
                        {
                            ResultType = StatusResultType.Info
                        };
                }
                catch (Exception ex)
                {
                    LogHelper.Error<XmlSitemapSizeCheck>("An error occurred trying to retrieve the sitemap.xml file", ex);

                    return new HealthCheckStatus(
                        $"{HealthCheckContext.SiteUrl}/sitemap.xml returned an error. Please check your sitemap implementation.")
                    {
                        ResultType = StatusResultType.Error
                    };
                }
            }
        }
    }
}
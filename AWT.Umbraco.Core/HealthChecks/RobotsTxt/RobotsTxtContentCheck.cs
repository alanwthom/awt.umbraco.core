﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Umbraco.Core.Logging;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.RobotsTxt
{
    [HealthCheck("fb7a9e47-2b7c-4a3e-8ffd-adf86be85b32",
                "Robots.txt contents",
                Description = "Checks the contents of the robots.txt file",
                Group = "Robots.txt")]
    public class RobotsTxtContentCheck : HealthCheck
    {
        public RobotsTxtContentCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckRobotsTxtContent() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }



        private HealthCheckStatus CheckRobotsTxtContent()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, $"{HealthCheckContext.SiteUrl}/robots.txt");
                    var response = client.SendAsync(request);

                    return new HealthCheckStatus($"{HealthCheckContext.SiteUrl}/robots.txt content: <strong>{response.Result.Content}</strong>.")
                    {
                        ResultType = StatusResultType.Info
                    };
                }
                catch (Exception ex)
                {
                    LogHelper.Error<RobotsTxtContentCheck>("An error occurred trying to retrieve the robots.txt file", ex);

                    return new HealthCheckStatus($"{HealthCheckContext.SiteUrl}/robots.txt returned an error. Please check your robots.txt implementation.")
                    {
                        ResultType = StatusResultType.Error
                    };
                }
            }
        }
    }
}
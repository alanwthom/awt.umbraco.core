﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Umbraco.Core.Logging;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.RobotsTxt
{
    [HealthCheck("15a67bab-741c-40e0-91b3-8715f25c1d30",
                "Robots.txt Exists",
                Description = "Checks to see if a robots.txt file exists",
                Group = "Robots.txt")]
    public class RobotsTxtExistsCheck : HealthCheck
    {
        public RobotsTxtExistsCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckRobotsTxtExists() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }



        private HealthCheckStatus CheckRobotsTxtExists()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Head, $"{HealthCheckContext.SiteUrl}/robots.txt");
                    var response = client.SendAsync(request);

                    return new HealthCheckStatus($"{HealthCheckContext.SiteUrl}/robots.txt returned a <strong>{(int)response.Result.StatusCode} {response.Result.StatusCode}</strong> status code.")
                    {
                        ResultType = response.Result.IsSuccessStatusCode ? StatusResultType.Success : StatusResultType.Error
                    };
                }
                catch (Exception ex)
                {
                    LogHelper.Error<RobotsTxtExistsCheck>("An error occurred trying to retrieve the robots.txt file", ex);

                    return new HealthCheckStatus($"{HealthCheckContext.SiteUrl}/robots.txt returned an error. Please check your robots.txt implementation.")
                    {
                        ResultType = StatusResultType.Error
                    };
                }
            }
        }
    }
}
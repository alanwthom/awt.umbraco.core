﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Templates
{
    [HealthCheck(
             "d0dd4380-eb6e-4bd0-9520-3818dff70227",
             "Check all templates",
             Description = "Makes an HTTP request to one instance of each active template to check its response code.",
             Group = "Templates")]
    public class TemplateCheck : HealthCheck
    {
        private readonly ServiceContext _services;
        private List<IContent> _templatesIndex;

        public TemplateCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext)
        {
            _services = HealthCheckContext.ApplicationContext.Services;
            _templatesIndex = new List<IContent>();
        }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return CheckTemplates();
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }



        private IEnumerable<HealthCheckStatus> CheckTemplates()
        {
            var statuses = new List<HealthCheckStatus>();
            var roots = _services.ContentService.GetRootContent();

            foreach (var node in roots)
            {
                AddNodeToTemplateList(node);
                CheckAllSubNodes(node);
            }

            foreach (var item in _templatesIndex)
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        var request = new HttpRequestMessage(HttpMethod.Head, $"{HealthCheckContext.SiteUrl}/{item.Id}");
                        var response = client.SendAsync(request);

                        statuses.Add(
                            new HealthCheckStatus($"Template: <strong>{item.Template.Alias}</strong><br/>Node: <strong><a href='/umbraco#/content/content/edit/{item.Id}'>{item.Name} ({item.Id})</a></strong><br/>Status: <strong>{(int)response.Result.StatusCode} {response.Result.StatusCode}</strong>")
                            {
                                ResultType = response.Result.IsSuccessStatusCode ? StatusResultType.Success : StatusResultType.Error
                            }
                        );
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error<TemplateCheck>($"An error occurred trying to retrieve {HealthCheckContext.SiteUrl}/{item.Id}", ex);

                        statuses.Add(
                            new HealthCheckStatus($"{item.Template.Alias}, Node: {item.Name} {item.Id}, Status: error")
                            {
                                ResultType = StatusResultType.Error
                            }
                        );
                    }
                }
            }

            return statuses.OrderByDescending(x => x.ResultType).ThenBy(x => x.Message);
        }

        private void CheckAllSubNodes(IContent node)
        {
            AddNodeToTemplateList(node);

            foreach (var child in node.Children())
            {
                CheckAllSubNodes(child);
            }
        }

        private void AddNodeToTemplateList(IContent node)
        {
            if (node.Published && !_templatesIndex.Any(x => x.Template.Equals(node.Template)))
            {
                _templatesIndex.Add(node);
            }
        }
    }
}
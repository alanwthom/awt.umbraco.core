﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Umbraco.Core.Logging;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    public static class SecurityCheckHelper
    {
        public static HealthCheckStatus DoHttpRequestHealthCheck(string url, string headerKey, string headerRecommendedValue, string headerReadableKey, string headerExplanation)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Head, $"{url}");
                    var response = client.SendAsync(request);

                    var headerExists = response.Result.DoHeadersContainKey(headerKey);
                    var headerValue = response.Result.GetHeadersValueByKey(headerKey);
                    var message = headerExists ?
                        (
                            headerValue.Contains(headerRecommendedValue) ?
                                $"A {headerReadableKey} header which {headerExplanation} was found: <strong>{headerValue}</strong>" :
                                $"A {headerReadableKey} header which {headerExplanation} was found: <strong>{headerValue}</strong></br></br>The recommended setting is <strong>{headerRecommendedValue}</strong>"
                        )
                        :
                        $"No X Xss Protection header which {headerExplanation} was found.</br></br>The recommended setting is <strong>{headerRecommendedValue}</strong>";

                    return new HealthCheckStatus(message)
                    {
                        ResultType = headerExists ? (headerValue.Contains(headerRecommendedValue) ? StatusResultType.Success : StatusResultType.Warning) : StatusResultType.Error
                    };
                }
                catch (Exception ex)
                {
                    LogHelper.Error<HealthCheck>($"An error occurred trying to retrieve the {headerReadableKey} header", ex);

                    return new HealthCheckStatus($"{url} returned an error.")
                    {
                        ResultType = StatusResultType.Error
                    };
                }
            }
        }


        public static bool DoHeadersContainKey(this HttpResponseMessage response, string key)
        {
            return response.Headers.Contains(key);
        }

        public static string GetContentHeadersValueByKey(this HttpResponseMessage response, string key, IEnumerable<string> defaultValue = null)
        {
            response.Content.Headers.TryGetValues(key, out defaultValue);
            return string.Join("", defaultValue);
        }

        public static string GetHeadersValueByKey(this HttpResponseMessage response, string key, IEnumerable<string> defaultValue = null)
        {
            response.Headers.TryGetValues(key, out defaultValue);
            return string.Join("", defaultValue);
        }
    }
}
﻿using System.Collections.Generic;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    [HealthCheck(
        "841b783a-1a7b-4ebc-a243-f8de877511ad",
        "Referrer Policy",
        Description = "Checks for a Referrer Policy header.",
        Group = "Security")]
    public class ReferrerPolicyCheck : HealthCheck
    {
        private const string HeaderKey = "Referrer-Policy";
        private const string HeaderRecommendedValue = "strict-origin-when-cross-origin";
        private const string HeaderReadableKey = "Referrer Policy";
        private const string HeaderExplanation = "is used to control the value of the referer header";

        public ReferrerPolicyCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckForReferrerPolicy() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new System.NotImplementedException();
        }

        private HealthCheckStatus CheckForReferrerPolicy()
        {
            return SecurityCheckHelper.DoHttpRequestHealthCheck(HealthCheckContext.SiteUrl, HeaderKey, HeaderRecommendedValue, HeaderReadableKey, HeaderExplanation);
        }
    }
}
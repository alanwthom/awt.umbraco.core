﻿using System.Collections.Generic;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    [HealthCheck(
        "841b793a-1a7b-4ebc-a243-f8de877511ad",
        "X Permitted Cross Domain Policies",
        Description = "Checks for a X Permitted Cross Domain Policies header.",
        Group = "Security")]
    public class XPermittedCrossDomainPoliciesCheck : HealthCheck
    {
        private const string HeaderKey = "X-Permitted-Cross-Domain-Policies";
        private const string HeaderRecommendedValue = "none";
        private const string HeaderReadableKey = "X Permitted Cross Domain Policies";
        private const string HeaderExplanation = "specifies whether a web client can handle data across domains";

        public XPermittedCrossDomainPoliciesCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckForXPermittedCrossDomainPolicies() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new System.NotImplementedException();
        }

        private HealthCheckStatus CheckForXPermittedCrossDomainPolicies()
        {
            return SecurityCheckHelper.DoHttpRequestHealthCheck(HealthCheckContext.SiteUrl, HeaderKey, HeaderRecommendedValue, HeaderReadableKey, HeaderExplanation);
        }
    }
}
﻿using System.Collections.Generic;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    [HealthCheck(
        "841b783a-3a7b-4ebc-a243-f8de877511ad",
        "X UA Compatible",
        Description = "Checks for a X UA Compatible header.",
        Group = "Security")]
    public class XUaCompatibleCheck : HealthCheck
    {
        private const string HeaderKey = "X-UA-Compatible";
        private const string HeaderRecommendedValue = "IE=edge";
        private const string HeaderReadableKey = "X UA Compatible";
        private const string HeaderExplanation = "is used to tell IE which compatibility mode to use";

        public XUaCompatibleCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckForXUaCompatible() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new System.NotImplementedException();
        }

        private HealthCheckStatus CheckForXUaCompatible()
        {
            return SecurityCheckHelper.DoHttpRequestHealthCheck(HealthCheckContext.SiteUrl, HeaderKey, HeaderRecommendedValue, HeaderReadableKey, HeaderExplanation);
        }
    }
}
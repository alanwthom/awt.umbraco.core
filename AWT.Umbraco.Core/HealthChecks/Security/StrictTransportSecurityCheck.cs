﻿using System.Collections.Generic;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    [HealthCheck(
        "841b783a-1a7b-4ebc-a243-f8de877511ad",
        "HTTP Strict Transport Security (HSTS)",
        Description = "Checks for a Strict Transport Security header.",
        Group = "Security")]
    public class StrictTransportSecurityCheck : HealthCheck
    {
        private const string HeaderKey = "Strict-Transport-Security";
        private const string HeaderRecommendedValue = "max-age=31536000; includeSubDomains; preload";
        private const string HeaderReadableKey = "HTTP Strict Transport Security (HSTS)";
        private const string HeaderExplanation = "is used to force a HTTPS connection";

        public StrictTransportSecurityCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckForStrictTransportSecurity() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new System.NotImplementedException();
        }
        
        private HealthCheckStatus CheckForStrictTransportSecurity()
        {
            return SecurityCheckHelper.DoHttpRequestHealthCheck(HealthCheckContext.SiteUrl, HeaderKey, HeaderRecommendedValue, HeaderReadableKey, HeaderExplanation);
        }
    }
}
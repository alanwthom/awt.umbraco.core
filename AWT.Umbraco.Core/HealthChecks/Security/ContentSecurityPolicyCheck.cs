﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Umbraco.Core.Logging;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    [HealthCheck(
        "831b783a-1a7b-4ebc-a243-f8de877511ad",
        "Content Security Policy",
        Description = "Checks for a Content Security Policy header.",
        Group = "Security")]
    public class ContentSecurityPolicyCheck : HealthCheck
    {
        private const string HeaderKey = "Content-Security-Policy";

        public ContentSecurityPolicyCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckForContentSecurityPolicy() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }



        private HealthCheckStatus CheckForContentSecurityPolicy()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var request = new HttpRequestMessage(HttpMethod.Head, $"{HealthCheckContext.SiteUrl}");
                    var response = client.SendAsync(request);

                    var headerExists = response.Result.DoHeadersContainKey(HeaderKey);
                    var headerValue = response.Result.GetHeadersValueByKey(HeaderKey);
                    var message = headerExists ? $"A Content Security Policy header which is used to prevent XSS attacks was found. <br/></br> {headerValue.Replace(";", ";<br/>")}" : "No Content Security Policy header which is used to prevent XSS attacks was found.";

                    return new HealthCheckStatus(message)
                    {
                        ResultType = headerExists ? StatusResultType.Info : StatusResultType.Error
                    };
                }
                catch (Exception ex)
                {
                    LogHelper.Error<ContentSecurityPolicyCheck>("An error occurred trying to retrieve the Content Security Policy header", ex);

                    return new HealthCheckStatus($"{HealthCheckContext.SiteUrl} returned an error.")
                    {
                        ResultType = StatusResultType.Error
                    };
                }
            }
        }
    }
}
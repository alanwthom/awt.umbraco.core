﻿using System.Collections.Generic;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    [HealthCheck(
        "841b783a-1a7b-4ebc-a213-f8de877511ad",
        "X Content Type Options",
        Description = "Checks for a X Content Type Options header.",
        Group = "Security")]
    public class XContentTypeOptionsCheck : HealthCheck
    {
        private const string HeaderKey = "X-Content-Type-Options";
        private const string HeaderRecommendedValue = "nosniff";
        private const string HeaderReadableKey = "X Content Type Options";
        private const string HeaderExplanation = "is used to control the value of the referer header";

        public XContentTypeOptionsCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckForXContentTypeOptions() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new System.NotImplementedException();
        }
        
        private HealthCheckStatus CheckForXContentTypeOptions()
        {
            return SecurityCheckHelper.DoHttpRequestHealthCheck(HealthCheckContext.SiteUrl, HeaderKey, HeaderRecommendedValue, HeaderReadableKey, HeaderExplanation);
        }
    }
}
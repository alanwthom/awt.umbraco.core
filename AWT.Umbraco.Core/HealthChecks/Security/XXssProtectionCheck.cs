﻿using System.Collections.Generic;
using Umbraco.Web.HealthCheck;

namespace AWT.Umbraco.Core.HealthChecks.Security
{
    [HealthCheck(
        "841b783a-3a7b-4ebc-a243-f8de877591ad",
        "X Xss Protection",
        Description = "Checks for a X Xss Protection header.",
        Group = "Security")]
    public class XXssProtectionCheck : HealthCheck
    {
        private const string HeaderKey = "X-XSS-Protection";
        private const string HeaderRecommendedValue = "1; mode=block";
        private const string HeaderReadableKey = "X XS Protection";
        private const string HeaderExplanation = "is used to block XSS attacks";

        public XXssProtectionCheck(HealthCheckContext healthCheckContext) : base(healthCheckContext) { }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            return new[] { CheckForXUaCompatible() };
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new System.NotImplementedException();
        }
        
        private HealthCheckStatus CheckForXUaCompatible()
        {
            return SecurityCheckHelper.DoHttpRequestHealthCheck(HealthCheckContext.SiteUrl, HeaderKey, HeaderRecommendedValue, HeaderReadableKey, HeaderExplanation);
        }
    }
}
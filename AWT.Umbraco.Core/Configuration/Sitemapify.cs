﻿using Sitemapify;
using Sitemapify.Umbraco;
using Umbraco.Core;

namespace AWT.Umbraco.Core.Configuration
{
    public class Sitemapify : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            if (AppSettings.SitemapifyEnabled)
            {
                Configure.With(config => config
                    .UsingContentProvider(new SitemapifyUmbracoContentProvider())
                );
            }
        }

        protected override bool ExecuteWhenApplicationNotConfigured { get; } = false;
        protected override bool ExecuteWhenDatabaseNotConfigured { get; } = false;
    }
}
﻿using System.ComponentModel;
using System.Configuration;

namespace AWT.Umbraco.Core
{
    public static class Helpers
    {
        public static T ConvertTo<T>(string name, T defaultValue)
        {
            try
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(name);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static T GetAppSetting<T>(string name, T defaultValue)
        {
            try
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(ConfigurationManager.AppSettings[name]);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static T GetPropertyWithFallback<T>(T target, T fallback)
        {
            if (typeof(T) == typeof(string))
            {
                if (string.IsNullOrEmpty((string)(object)target)) {
                    return fallback;
                }

                return target;
            } 
             
            if (target == null)
            {
                return fallback;
            }

            return target;
        }
    }
}

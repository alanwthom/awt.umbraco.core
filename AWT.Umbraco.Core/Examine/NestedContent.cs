﻿using System.Collections.Generic;
using System.Linq;
using Examine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Umbraco.Core;

namespace AWT.Umbraco.Core.Examine
{
    public class NestedContent : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            if (!string.IsNullOrEmpty(AppSettings.SearchIndexer))
            {
                ExamineManager.Instance.IndexProviderCollection[AppSettings.SearchIndexer].GatheringNodeData += IndexNestedContent;
            }
        }



        private void IndexNestedContent(object sender, IndexingNodeDataEventArgs e)
        {
            var fieldKeys = e.Fields.Keys.ToArray();
            foreach (var key in fieldKeys)
            {
                var value = e.Fields[key];
                if (value.DetectIsJson())
                {
                    IndexNestedObject(e.Fields, JsonConvert.DeserializeObject(value), key);
                }
            }
        }

        private void IndexNestedObject(Dictionary<string, string> fields, object obj, string prefix)
        {
            var objType = obj.GetType();
            if (objType == typeof(JObject))
            {
                var jObj = obj as JObject;
                if (jObj != null)
                {
                    foreach (var kvp in jObj)
                    {
                        var propKey = prefix + "_" + kvp.Key;
                        var valueType = kvp.Value.GetType();
                        if (typeof(JContainer).IsAssignableFrom(valueType))
                        {
                            IndexNestedObject(fields, kvp.Value, propKey);
                        }
                        else
                        {
                            fields.Add(propKey, kvp.Value.ToString().StripHtml());
                        }
                    }
                }
            }
            else if (objType == typeof(JArray))
            {
                var jArr = obj as JArray;
                if (jArr != null)
                {
                    for (var i = 0; i < jArr.Count; i++)
                    {
                        var itm = jArr[i];
                        var propKey = prefix + "_" + i;
                        var valueType = itm.GetType();
                        if (typeof(JContainer).IsAssignableFrom(valueType))
                        {
                            IndexNestedObject(fields, itm, propKey);
                        }
                        else
                        {
                            fields.Add(propKey, itm.ToString().StripHtml());
                        }
                    }
                }
            }
        }
    }
}
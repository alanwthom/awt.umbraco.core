﻿using Umbraco.Core.Models;

namespace AWT.Umbraco.Core.Extensions
{
    public static class PublishedContentExtensions
    {
        public static bool IsPathActive(this IPublishedContent current, IPublishedContent target)
        {
            return current.Path.Contains(target.Id.ToString());
        }
    }
}
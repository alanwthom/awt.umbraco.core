﻿using AWT.Umbraco.Core.Models;

namespace AWT.Umbraco.Core.Extensions
{
    public static class ListModelExtensions
    {
        public static string GetUrl(this ListModel model, string url, int page = 1, int size = 10, int offset = 0)
        {
            return
                $"{url}?" +
                $"{(string.IsNullOrEmpty(model.Query) ? "" : $"q={model.Query}&")}" +
                $"{(offset == 0 ? "" : $"o={model.Offset}&")}" +
                $"{(page == 1 ? "" : $"p={model.Page}&")}" +
                $"{(size == 10 ? "" : $"s={model.Size}")}";
        }
    }
}
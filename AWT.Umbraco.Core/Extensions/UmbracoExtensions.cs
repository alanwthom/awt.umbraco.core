﻿using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace AWT.Umbraco.Core.Extensions
{
    public static class UmbracoExtensions
    {
        public static IPublishedContent GetTypedMedia(this UmbracoHelper uHelper, string id)
        {
            var media = uHelper.TypedMedia(id);

            if (media == null)
            {
                GuidUdi udi = null;
                GuidUdi.TryParse(id, out udi);

                media = udi == null ? media : uHelper.TypedMedia(udi);
            }

            return media;
        }
    }
}
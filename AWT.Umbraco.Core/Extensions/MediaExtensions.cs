﻿using ByteSizeLib;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace AWT.Umbraco.Core.Extensions
{
    public static class MediaExtensions
    {
        public static string GetAltText(this IPublishedContent content)
        {
            return string.IsNullOrEmpty(content.GetPropertyValue<string>("altText"))
                ? string.Empty
                : content.GetPropertyValue<string>("altText");
        }

        public static string GetCdnUrl(this IPublishedContent content)
        {
            return content.ItemType == PublishedItemType.Media ? $"{AppSettings.AzureBlobCacheCachedCdnRoot}{content.Url}" : content.Url;
        }

        public static string GetSize(this IPublishedContent content)
        {
            var strFileSize = content.GetPropertyValue<string>("umbracoBytes");
            var intFileSize = Helpers.ConvertTo(strFileSize, 0.0);
            return new ByteSize(intFileSize).ToString("#.#");
        }

        public static string GetExtension(this IPublishedContent content)
        {
            return content.HasProperty("umbracoExtension") ? content.GetPropertyValue<string>("umbracoExtension").ToLower() : null;
        }
    }
}
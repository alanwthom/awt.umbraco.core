﻿using System.Linq;
using System.Web;
using HtmlAgilityPack;

namespace AWT.Umbraco.Core.Extensions
{
    public static class HtmlStringExtensions
    {
        public static HtmlString Clean(this IHtmlString html)
        {
            if (string.IsNullOrEmpty(html.ToHtmlString()))
            {
                return new HtmlString("");
            }

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html.ToHtmlString());

            htmlDocument = htmlDocument.RemoveFontTags();
            htmlDocument = htmlDocument.RemoveStyleAttributes();
            htmlDocument = htmlDocument.RemoveStyleTags();

            return new HtmlString(htmlDocument.DocumentNode.InnerHtml);
        }



        private static HtmlDocument RemoveFontTags(this HtmlDocument htmlDocument)
        {
            var nodes = htmlDocument.DocumentNode.SelectNodes("//font");

            if (nodes != null && nodes.Any())
            {
                foreach (var node in nodes)
                {
                    node.Remove();
                }
            }

            return htmlDocument;
        }

        private static HtmlDocument RemoveStyleAttributes(this HtmlDocument htmlDocument)
        {
            var nodes = htmlDocument.DocumentNode.SelectNodes("//*[@style]");

            if (nodes != null && nodes.Any())
            {
                foreach (var node in nodes)
                {
                    node.Attributes.Remove("style");
                }
            }

            return htmlDocument;
        }

        private static HtmlDocument RemoveStyleTags(this HtmlDocument htmlDocument)
        {
            var nodes = htmlDocument.DocumentNode.SelectNodes("//style");

            if (nodes != null && nodes.Any())
            {
                foreach (var node in nodes)
                {
                    node.Remove();
                }
            }

            return htmlDocument;
        }
    }
}
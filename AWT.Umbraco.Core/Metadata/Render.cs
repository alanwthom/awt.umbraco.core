﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace AWT.Umbraco.Core.Metadata
{
    public static class Render
    {
        public static IHtmlString RenderMetadata(this HtmlHelper html, IPublishedContent content)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"<title>{Helpers.GetPropertyWithFallback((string)content.GetProperty(AppSettings.MetaTitleTag).Value, AppSettings.ApplicationName)}</title>");
            sb.AppendLine($@"<meta name=""application-name"" content=""{AppSettings.ApplicationName}"" />");
            sb.AppendLine($@"<meta name=""description"" content=""{(string)content.GetProperty(AppSettings.MetaMetaDescription).Value}"" />");
            sb.AppendLine($@"<meta property=""og:site_name"" content=""{AppSettings.ApplicationName}"" />");
            sb.AppendLine($@"<meta property=""og:title"" content=""{content.GetOgTitle()}"" />");
            sb.AppendLine($@"<meta property=""og:url"" content=""{content.GetCanonicalUrl()}"" />");
            sb.AppendLine($@"<meta property=""og:description"" content=""{content.GetOgDescription()}"" />");
            sb.AppendLine($@"<meta property=""og:image"" content=""{content.GetOgImage()}"" />");
            sb.AppendLine($@"<meta name=""twitter:card"" content=""{AppSettings.TwitterCard}"" />");
            sb.AppendLine($@"<meta name=""twitter:creator"" content=""{AppSettings.TwitterUsername}"" />");
            sb.AppendLine($@"<meta name=""twitter:title"" content=""{content.GetOgTitle()}"" />");
            sb.AppendLine($@"<meta name=""twitter:description"" content=""{content.GetOgDescription()}"" />");
            sb.AppendLine($@"<meta name=""twitter:image"" content=""{content.GetOgImage()}"" />");
            sb.AppendLine($@"<link rel=""canonical"" href=""{content.GetCanonicalUrl()}"" />");

            if (!string.IsNullOrEmpty(AppSettings.GoogleSiteVerificationKey))
            {
                sb.AppendLine($@"<meta name=""google-site-verification"" content=""{AppSettings.GoogleSiteVerificationKey}"" />");
            }

            if (AppSettings.RobotifyEnabled && string.IsNullOrEmpty(AppSettings.RobotifyDisallowPaths) || !string.IsNullOrEmpty(AppSettings.MetaRobotsAlias) && (bool)content.GetProperty(AppSettings.MetaRobotsAlias).Value)
            {
                sb.AppendLine($@"<meta name=""robots"" content=""noindex"" />");
            }

            return new HtmlString(sb.ToString());
        }



        private static string GetCanonicalUrl(this IPublishedContent content)
        {
            return Helpers.GetPropertyWithFallback((string)content.GetProperty(AppSettings.MetaCanonicalUrl).Value, content.UrlWithDomain());
        }

        private static string GetOgImage(this IPublishedContent content)
        {
            var imageUrl = AppSettings.MetaOgImageFallbackPath;

            if (content.HasProperty(AppSettings.MetaOgImageOverride) && content.GetProperty(AppSettings.MetaOgImageOverride).HasValue)
            {
                imageUrl = ((IPublishedContent)content.GetProperty(AppSettings.MetaOgImageOverride).Value).GetCropUrl(AppSettings.MetaOgImageCrop);
            }

            if (content.HasProperty(AppSettings.MetaOgImage) && content.GetProperty(AppSettings.MetaOgImage).HasValue)
            {
                imageUrl = ((IPublishedContent)content.GetProperty(AppSettings.MetaOgImage).Value).GetCropUrl(AppSettings.MetaOgImageCrop);
            }

            return $"{AppSettings.AzureBlobCacheCachedCdnRoot}{imageUrl}";
        }

        private static string GetOgTitle(this IPublishedContent content)
        {
            return Helpers.GetPropertyWithFallback((string)content.GetProperty(AppSettings.MetaOgTitle).Value, (string)content.GetProperty(AppSettings.MetaTitleTag).Value);
        }

        private static string GetOgDescription(this IPublishedContent content)
        {
            return Helpers.GetPropertyWithFallback((string)content.GetProperty(AppSettings.MetaOgDescription).Value, (string)content.GetProperty(AppSettings.MetaMetaDescription).Value);
        }
    }
}

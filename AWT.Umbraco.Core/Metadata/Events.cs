﻿using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace AWT.Umbraco.Core.Metadata
{
    public class Events : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Saving += Saving;
            ContentService.Publishing += Publishing;
        }

        void Saving(IContentService sender, SaveEventArgs<IContent> e)
        {
            foreach (var node in e.SavedEntities)
            {
                SetMetaProperties(node);
            }
        }

        void Publishing(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            foreach (var node in e.PublishedEntities)
            {
                SetMetaProperties(node);
            }
        }



        private void SetMetaProperties(IContent node)
        {
            var home = new UmbracoHelper(UmbracoContext.Current).TypedContentAtRoot().FirstOrDefault();

            if (home != null)
            {
                if (node.HasProperty(AppSettings.MetaTitleTag))
                {
                    node.SetValue(AppSettings.MetaTitleTag, home.Id == node.Id ? $"{AppSettings.ApplicationName} — {node.Name}" : $"{node.Name} — {AppSettings.ApplicationName}");
                }

                if (node.HasProperty(AppSettings.MetaOgTitle))
                {
                    node.SetValue(AppSettings.MetaOgTitle, home.Id == node.Id ? $"{AppSettings.ApplicationName} — {node.Name}" : $"{node.Name} — {AppSettings.ApplicationName}");
                }
            }
        }
    }
}

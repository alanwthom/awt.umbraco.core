﻿using System;
using System.Collections.Generic;
using System.Linq;
using AWT.Umbraco.Core.Models;
using Examine;
using Examine.LuceneEngine.SearchCriteria;
using Examine.SearchCriteria;
using Umbraco.Core.Models;
using Umbraco.Web;
using UmbracoExamine;

namespace AWT.Umbraco.Core.Services
{
    public class SearchService
    {
        public ListModel FuzzySearch(string query, int page, int size, string fields)
        {
            var results = GetResults(query, fields);
            var uHelper = new UmbracoHelper(UmbracoContext.Current);

            return new ListModel
            {
                Query = query,
                Page = page,
                Size = size,
                TotalPages = (int)Math.Ceiling((double)results.Count() / (double)size),
                Count = results.Count(),
                Results = results.Any() ? results.Skip((page - 1) * size).Take(size).Select(x => uHelper.TypedContent(x.Id)) : Enumerable.Empty<IPublishedContent>()
            };
        }

        private IEnumerable<SearchResult> GetResults(string query,  string fields)
        {
            if (string.IsNullOrEmpty(query))
            {
                return Enumerable.Empty<SearchResult>();
            }

            var criteria = ExamineManager.Instance.SearchProviderCollection[AppSettings.SearchSearcher].CreateSearchCriteria(IndexTypes.Content);

            IBooleanOperation filters = criteria.GroupedOr(fields.Split(','), query.Split(' ').First().Fuzzy());
            filters = query.Split(' ').Skip(1).Aggregate(filters, (current, q) => current.Or().GroupedOr(AppSettings.SearchFields.Split(','), q.Fuzzy()));

            return ExamineManager.Instance.SearchProviderCollection[AppSettings.SearchSearcher]
                .Search(filters.Compile())
                .OrderByDescending(x => x.Score)
                .Distinct()
                .TakeWhile(x => x.Score > 0);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace AWT.Umbraco.Core
{
    public static class AppSettings
    {
        // App Insights
        public static string ApplicationInsightsKey => Helpers.GetAppSetting("ApplicationInsights:InstrumentationKey", "");
        public static bool ApplicationInsightsEnabled => !Helpers.GetAppSetting("ApplicationInsights:Disabled", true);


        //Azure Blob Storage
        public static string AzureBlobCacheCachedCdnRoot => Helpers.GetAppSetting("ImageProcessor.AzureBlobCache.CachedCDNRoot", "").TrimEnd('/');
        public static string AzureBlobFileSystemRootUrl => Helpers.GetAppSetting("AzureBlobFileSystem.RootUrl:media", "").TrimEnd('/');


        // Google Site Verification
        public static string GoogleSiteVerificationKey => Helpers.GetAppSetting("GoogleSiteVerificationKey", "");

        // Google Tag Manager
        public static bool GoogleTagManagerEnabled => !Helpers.GetAppSetting("GoogleTagManager:Disabled", true);
        public static string GoogleTagManagerKey => Helpers.GetAppSetting("GoogleTagManager:Key", "");
        

        // Metadata
        public static string MetaTitleTag => Helpers.GetAppSetting("MetaTitleTag", "");
        public static string MetaMetaDescription => Helpers.GetAppSetting("MetaMetaDescription", "");
        public static string MetaOgTitle => Helpers.GetAppSetting("MetaOgTitle", "");
        public static string MetaOgDescription => Helpers.GetAppSetting("MetaOgDescription", "");
        public static string MetaOgImage => Helpers.GetAppSetting("MetaOgImage", "");
        public static string MetaOgImageOverride => Helpers.GetAppSetting("MetaOgImageOverride", "");
        public static string MetaOgImageFallbackPath => Helpers.GetAppSetting("MetaOgImageFallbackPath", "");
        public static string MetaOgImageCrop => Helpers.GetAppSetting("MetaOgImageCrop", "");
        public static string MetaCanonicalUrl => Helpers.GetAppSetting("MetaCanonicalUrl", "");
        public static string MetaRobotsAlias => Helpers.GetAppSetting("MetaRobotsAlias", "");


        // Robotify
        public static bool RobotifyEnabled => Helpers.GetAppSetting("Robotify:Enabled", true);
        public static string RobotifyDisallowPaths => Helpers.GetAppSetting("Robotify:DisallowPaths", "");


        // Search
        public static string SearchFields => Helpers.GetAppSetting("Search:Fields", "");
        public static string SearchIndexer => Helpers.GetAppSetting("Search:Indexer", "");
        public static string SearchSearcher => Helpers.GetAppSetting("Search:Searcher", "");


        // Sitemapify
        public static bool SitemapifyEnabled => !Helpers.GetAppSetting("Sitemapify.Umbraco:Disabled", false);


        // Twitter
        public static string TwitterCard => Helpers.GetAppSetting("TwitterCard", "");
        public static string TwitterUsername => Helpers.GetAppSetting("TwitterUsername", "");


        // Misc
        public static string ApplicationName => Helpers.GetAppSetting("ApplicationName", "");
        public static string BaseUrl => Helpers.GetAppSetting("BaseUrl", "").TrimEnd('/');
        public static IEnumerable<int> ProtectedNodeIds => Helpers.GetAppSetting("ProtectedNodeIds", "").Split(',').Select(x => Helpers.ConvertTo(x, 0));
    }
}

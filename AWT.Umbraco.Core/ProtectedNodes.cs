﻿using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;

namespace AWT.Umbraco.Core
{
    public class ProtectedNodes : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Trashing += Trashing;
            ContentService.UnPublishing += Unpublishing;
            
        }

        private void Trashing(IContentService sender, MoveEventArgs<IContent> e)
        {
            foreach (var item in e.MoveInfoCollection)
            {
                if (AppSettings.ProtectedNodeIds.Contains(item.Entity.Id))
                {
                    e.Cancel = true;
                    e.Messages.Add(new EventMessage("Warning", $"Cannot delete {item.Entity.Name}"));
                    break;
                }
            }
        }

        private void Unpublishing(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            foreach (var item in e.PublishedEntities)
            {
                if (AppSettings.ProtectedNodeIds.Contains(item.Id))
                {
                    e.Cancel = true;
                    e.Messages.Add(new EventMessage("Warning", $"Cannot delete {item.Name}"));
                    break;
                }
            }
        }
    }
}
﻿namespace AWT.Umbraco.Core.Models
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true,
        Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9",
        IsNullable = false)]
    public partial class urlset
    {
        private urlsetUrl[] urlField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("url")]
        public urlsetUrl[] url
        {
            get { return this.urlField; }
            set { this.urlField = value; }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true,
        Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")]
    public partial class urlsetUrl
    {
        private string locField;

        /// <remarks/>
        public string loc
        {
            get { return this.locField; }
            set { this.locField = value; }
        }
    }
}
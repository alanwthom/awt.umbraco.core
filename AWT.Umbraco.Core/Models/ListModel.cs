﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace AWT.Umbraco.Core.Models
{
    public class ListModel
    {
        public int Size { get; set; }
        public int Offset { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public int Count { get; set; }
        public IEnumerable<IPublishedContent> Results { get; set; }
        public string Query { get; set; }

        public ListModel() { }
        public ListModel(IEnumerable<IPublishedContent> children, int size, int offset, int page)
        {
            Size = size;
            Offset = offset;
            Page = page;
            TotalPages = (int)Math.Ceiling((double)children.Count() / (double)size);
            Count = children.Count();
            Results = children.Skip((page - 1) * size).Take(size);
        }
    }
}